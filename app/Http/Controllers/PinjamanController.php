<?php

namespace App\Http\Controllers;

use App\Pinjaman;
use Illuminate\Http\Request;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pinjaman::all();
        return response()->json(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Pinjaman::create($request->all());

        return response()->json(compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pinjaman::find($id);

        return response()->json(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTglKembali(Request $request, $id)
    {
        $data = Pinjaman::where('id',$id)->update([
            'tanggal_pengembalian' => $request->tanggal_pengembalian
        ]);

        return response()->json(compact('data'));
    }

    public function updateStatusOntime(Request $request, $id)
    {
        $data = Pinjaman::where('id',$id)->update([
            'status_ontime' => $request->status_ontime
        ]);

        return response()->json(compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
