<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'username' => ['required','unique:users,username'],
            'email' => ['required','unique:users,email'],
            'role' => 'required',
            'password' => 'required'
        ]);

        User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'role' => $request->role,
            'password' => Hash::make($request->password),
        ]);

        

        return 'Registrasi berhasil dilakukan';
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if(!$token = auth()->attempt($request->only('email','password'))){
            return response(null, 401);
        }

        return response()->json(compact('token'));
    }

    public function logout()
    {
        auth()->logout();
    }
}
