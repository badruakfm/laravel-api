<?php

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');




Route::middleware('auth:api')->group(function () {
    Route::get('mahasiswa', 'MahasiswaController@index');
    Route::post('mahasiswa', 'MahasiswaController@store');
    Route::get('mahasiswa/{id}', 'MahasiswaController@show');
    Route::put('mahasiswa/{id}', 'MahasiswaController@update');
    Route::delete('mahasiswa/{id}', 'MahasiswaController@destroy');

    Route::get('buku', 'BukuController@index');
    Route::post('buku', 'BukuController@store');
    Route::get('buku/{id}', 'BukuController@show');
    Route::put('buku/{id}', 'BukuController@update');
    Route::delete('buku/{id}', 'BukuController@destroy');

    Route::get('pinjaman', 'PinjamanController@index');
    Route::post('pinjaman', 'PinjamanController@store');
    Route::get('pinjaman/{id}', 'PinjamanController@show');
});

Route::middleware(['auth:api','admin'])->group(function(){
    Route::put('pinjaman/ubah-tanggal-kembali/{id}', 'PinjamanController@updateTglKembali');
    Route::put('pinjaman/ubah-status-ontime/{id}', 'PinjamanController@updateStatusOntime');
});